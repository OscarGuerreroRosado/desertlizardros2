import rclpy
from rclpy.node import Node
from geometry_msgs.msg import PointStamped
from webots_ros2_msgs.msg import FloatStamped
from webots_msgs_pkg.msg import LocalViews, InternalState, AttOutput, SimulationOn

import matplotlib.pyplot as plt
import numpy as np
import math
import csv




plotting = True

arena_size = 250 + 10
arena_limit = 6
arenaX = [0,arena_size]
arenaY = [0,arena_size]
grid_size=1


if plotting == True:
    plt.ion()
    plt.style.use('seaborn')
    fig1, ax1 = plt.subplots(5, 1,figsize=(3,15))
    fig2, ax2 = plt.subplots(1, 2,figsize=(20,4))
    fig2.tight_layout(pad=2.0)


class Pseudo_Supervisor(Node):
    def __init__(self):
        super().__init__('pseudo_supervisor')

        self.get_logger().info('------------- ----------------- pseudo_supervisor -------------- -------------')

        self.create_subscription(PointStamped, '/my_epuck/gps', self.agent_GPS_callback, 1)
        self.create_subscription(FloatStamped, '/my_epuck/compass/bearing', self.agent_Compass_callback, 1)
        self.create_subscription(PointStamped, '/epuck_predator/gps', self.predator_GPS_callback, 1)
        self.create_subscription(FloatStamped, '/epuck_predator/compass/bearing', self.predator_Compass_callback, 1)
        self.create_subscription(PointStamped, '/epuck_peer/gps', self.peer_GPS_callback, 1)
        self.create_subscription(FloatStamped, '/epuck_peer/compass/bearing', self.peer_Compass_callback, 1)

        self.create_subscription(SimulationOn, '/allostatic_controller/SimulationRuning', self.simulation_running_callback, 1)
        self.create_subscription(InternalState, '/allostatic_controller/InternalState', self.internal_state_callback, 1)
        self.create_subscription(AttOutput, '/allostatic_controller/Attractor_outputs', self.attractor_outputs_callback, 1)

        self.LV__publisher = self.create_publisher(LocalViews, 'pseudo_supervisor/Local_views', 1)
        self.Simulation__publisher = self.create_publisher(SimulationOn, 'allostatic_controller/SimulationRuning', 1)

        self.timesteps_in_cycle = 200 #100.000
        self.cycles = 1
        self.max_cycles = 11
        self.night = False

        self.counter=0
        self.simulationOn = False

        self.agent_x_position = 0
        self.agent_y_position = 0
        self.agent_orientation = 0
        self.predator_x_gradient_position = 0
        self.predator_y_gradient_position = 0
        self.peer_x_gradient_position = 0
        self.peer_y_gradient_position = 0
        self.saving_data = False

        self.agent_x_position_list = []
        self.agent_y_position_list = []

        self.aVtemperature_list = []  
        self.aVthirst_list = []  
        self.aVfood_list = []  
        self.aVpeer_list = []  
        self.aVsecurity_list = []

        self.TFtemperature_list = []  
        self.TFthirst_list = []  
        self.TFfood_list = []  
        self.TFpeer_list = []  
        self.TFpredator_list = []

        self.meanGrad_Temp = []

        self.create_temp_gradient(1)
        self.create_thirst_gradient()
        self.create_food_gradient()
        self.create_predator_gradient()
        self.create_peer_gradient()
        self.build_gradients()

        self.timer = self.create_timer(0.05, self.parallel_loop_callback)

    def agent_GPS_callback(self, message):
        self.agent_x_position = message.point.x
        self.agent_y_position = message.point.y

        self.agent_x_position_list.append(self.agent_x_position)
        self.agent_y_position_list.append(self.agent_y_position)


    def agent_Compass_callback(self, message):
        self.agent_orientation = message.data


    def predator_GPS_callback(self, message):
        self.predator_x_position = message.point.x
        self.predator_y_position = message.point.y


    def predator_Compass_callback(self, message):
        self.predator_orientation = message.data


    def peer_GPS_callback(self, message):
        self.peer_x_position = message.point.x
        self.peer_y_position = message.point.y

    def peer_Compass_callback(self, message):
        self.peer_orientation = message.data

    def simulation_running_callback(self, message):
        self.simulationOn = message.simulation_running


    def internal_state_callback(self, message):
        self.temperature_IS = message.is_temperature
        self.thirst_IS = message.is_thirst
        self.food_IS = message.is_food
        self.predator_IS = message.is_predator
        self.peer_IS = message.is_peer

        if self.simulationOn == True:
            self.aVtemperature_list.append(self.temperature_IS)
            self.aVthirst_list.append(self.thirst_IS)
            self.aVfood_list.append(self.food_IS)
            self.aVsecurity_list.append(1 - self.predator_IS)
            self.aVpeer_list.append(self.peer_IS)

            self.meanGrad_Temp.append(self.temperature_intensity.mean())

    def attractor_outputs_callback(self, message):
        self.temperature_Att = message.att_temperature
        self.thirst_Att = message.att_thirst
        self.food_Att = message.att_food
        self.predator_Att = message.att_predator
        self.peer_Att = message.att_peer

        if self.simulationOn == True:
            self.TFtemperature_list.append(self.temperature_Att)
            self.TFthirst_list.append(self.thirst_Att)
            self.TFfood_list.append(self.food_Att)
            self.TFpredator_list.append(self.predator_Att)
            self.TFpeer_list.append(self.peer_Att)


########################### GRADIENTS FUNCTIONS ###########################
    def kde_quartic(self,d,h):
        dn=d/h
        P=(15/16)*(1-dn**2)**2
        return P


    def sigmoid(self, x, timestep):
        limit_xInter = 1
        increment_xInter = (limit_xInter*2)/self.timesteps_in_cycle
        slope = 15

        xInter = -limit_xInter + increment_xInter*timestep
        return 1 / (1 + math.exp(-slope * (x-xInter)))


    def create_temp_gradient(self, currentTstep):

        arena_axis = np.arange(-1 , 1, 0.0077) #0.008
        arena_axis = arena_axis[::-1]

        arena_column = []
        for i in range(arena_size):
            arena_column.append(self.sigmoid(arena_axis[i],currentTstep))

        for i in range(len(arena_column)):
            if arena_column[i]>0.98:
                arena_column[i] = 0.98

        temp_gradient = np.tile(arena_column, (arena_size,1))
        temp_gradient = temp_gradient.transpose()

        #CONSTRUCT GRID
        x_grid=np.arange(arenaX[0],arenaX[1],grid_size)
        y_grid=np.arange(arenaY[0],arenaY[1],grid_size)
        self.x_mesh_temperature,self.y_mesh_temperature=np.meshgrid(x_grid,y_grid)
        self.temperature_intensity = temp_gradient

        '''if self.simulationOn == True:
                                    self.meanGrad_Temp.append(self.temperature_intensity.mean())'''


    def create_thirst_gradient(self):
        self.thirst_intensity_list=[]

        #POINT DATASET
        x= [220]
        y= [220]
        #DEFINE GRID SIZE AND RADIUS(h)
        grid_size=1
        h=280
        #CONSTRUCT GRID
        x_grid=np.arange(arenaX[0],arenaX[1],grid_size)
        y_grid=np.arange(arenaY[0],arenaY[1],grid_size)
        self.x_mesh_thirst,self.y_mesh_thirst=np.meshgrid(x_grid,y_grid)
        #GRID CENTER POINT
        xc=self.x_mesh_thirst+(grid_size/2)
        yc=self.y_mesh_thirst+(grid_size/2)
        #PROCESSING
        for j in range(len(xc)):
            intensity_row=[]
            for k in range(len(xc[0])):
                kde_value_list=[]
                for i in range(len(x)):
                    #CALCULATE DISTANCE
                    d=math.sqrt((xc[j][k]-x[i])**2+(yc[j][k]-y[i])**2) 
                    if d<=h:
                        p=self.kde_quartic(d,h)
                    else:
                        p=0
                    kde_value_list.append(p)
                #SUM ALL INTENSITY VALUE
                p_total=sum(kde_value_list)
                intensity_row.append(p_total)
            self.thirst_intensity_list.append(intensity_row)


    def create_food_gradient(self):
        self.food_intensity_list=[]

        #POINT DATASET
        x= [30]
        y= [220]
        #DEFINE GRID SIZE AND RADIUS(h)
        grid_size=1
        h=280
        #CONSTRUCT GRID
        x_grid=np.arange(arenaX[0],arenaX[1],grid_size)
        y_grid=np.arange(arenaY[0],arenaY[1],grid_size)
        self.x_mesh_food,self.y_mesh_food=np.meshgrid(x_grid,y_grid)
        #GRID CENTER POINT
        xc=self.x_mesh_food+(grid_size/2)
        yc=self.y_mesh_food+(grid_size/2)
        #PROCESSING
        for j in range(len(xc)):
            intensity_row=[]
            for k in range(len(xc[0])):
                kde_value_list=[]
                for i in range(len(x)):
                    #CALCULATE DISTANCE
                    d=math.sqrt((xc[j][k]-x[i])**2+(yc[j][k]-y[i])**2) 
                    if d<=h:
                        p=self.kde_quartic(d,h)
                    else:
                        p=0
                    kde_value_list.append(p)
                #SUM ALL INTENSITY VALUE
                p_total=sum(kde_value_list)
                intensity_row.append(p_total)
            self.food_intensity_list.append(intensity_row)


    def create_predator_gradient(self):
        self.predator_intensity_list=[]

        #POINT DATASET
        x= [self.predator_x_gradient_position]
        y= [self.predator_y_gradient_position]
        #DEFINE GRID SIZE AND RADIUS(h)
        grid_size=1
        h=150
        #CONSTRUCT GRID
        x_grid=np.arange(arenaX[0],arenaX[1],grid_size)
        y_grid=np.arange(arenaY[0],arenaY[1],grid_size)
        self.x_mesh_predator,self.y_mesh_predator=np.meshgrid(x_grid,y_grid)
        #GRID CENTER POINT
        xc=self.x_mesh_predator+(grid_size/2)
        yc=self.y_mesh_predator+(grid_size/2)
        #PROCESSING
        for j in range(len(xc)):
            intensity_row=[]
            for k in range(len(xc[0])):
                kde_value_list=[]
                for i in range(len(x)):
                    #CALCULATE DISTANCE
                    d=math.sqrt((xc[j][k]-x[i])**2+(yc[j][k]-y[i])**2) 
                    if d<=h:
                        p=self.kde_quartic(d,h)
                    else:
                        p=0
                    kde_value_list.append(p)
                #SUM ALL INTENSITY VALUE
                p_total=sum(kde_value_list)
                intensity_row.append(p_total)
            self.predator_intensity_list.append(intensity_row)


    def create_peer_gradient(self):
        self.peer_intensity_list=[]

        #POINT DATASET
        x= [self.peer_x_gradient_position]
        y= [self.peer_y_gradient_position]
        #DEFINE GRID SIZE AND RADIUS(h)
        grid_size=1
        h=200
        #CONSTRUCT GRID
        x_grid=np.arange(arenaX[0],arenaX[1],grid_size)
        y_grid=np.arange(arenaY[0],arenaY[1],grid_size)
        self.x_mesh_peer,self.y_mesh_peer=np.meshgrid(x_grid,y_grid)
        #GRID CENTER POINT
        xc=self.x_mesh_peer+(grid_size/2)
        yc=self.y_mesh_peer+(grid_size/2)
        #PROCESSING
        for j in range(len(xc)):
            intensity_row=[]
            for k in range(len(xc[0])):
                kde_value_list=[]
                for i in range(len(x)):
                    #CALCULATE DISTANCE
                    d=math.sqrt((xc[j][k]-x[i])**2+(yc[j][k]-y[i])**2) 
                    if d<=h:
                        p=self.kde_quartic(d,h)
                    else:
                        p=0
                    kde_value_list.append(p)
                #SUM ALL INTENSITY VALUE
                p_total=sum(kde_value_list)
                intensity_row.append(p_total)
            self.peer_intensity_list.append(intensity_row)


    def build_gradients(self):

        thirst_min = 100
        thirst_max = 0
        for i in range(len(self.thirst_intensity_list)):
            for j in range(len(self.thirst_intensity_list[i])):
                if self.thirst_intensity_list[i][j] < thirst_min:
                    thirst_min = self.thirst_intensity_list[i][j]
                if self.thirst_intensity_list[i][j] > thirst_max:
                    thirst_max = self.thirst_intensity_list[i][j]

        thirst_intensity=np.array(self.thirst_intensity_list)
        self.thirst_intensity=thirst_intensity/thirst_max

        food_min = 100
        food_max = 0
        for i in range(len(self.food_intensity_list)):
            for j in range(len(self.food_intensity_list[i])):
                if self.food_intensity_list[i][j] < food_min:
                    food_min = self.food_intensity_list[i][j]
                if self.food_intensity_list[i][j] > food_max:
                    food_max = self.food_intensity_list[i][j]

        food_intensity=np.array(self.food_intensity_list)
        self.food_intensity=food_intensity/food_max

        predator_min = 100
        predator_max = 0
        for i in range(len(self.predator_intensity_list)):
            for j in range(len(self.predator_intensity_list[i])):
                if self.predator_intensity_list[i][j] < predator_min:
                    predator_min = self.predator_intensity_list[i][j]
                if self.predator_intensity_list[i][j] > predator_max:
                    predator_max = self.predator_intensity_list[i][j]

        predator_intensity=np.array(self.predator_intensity_list)
        self.predator_intensity=predator_intensity/predator_max


        peer_min = 100
        peer_max = 0
        for i in range(len(self.peer_intensity_list)):
            for j in range(len(self.peer_intensity_list[i])):
                if self.peer_intensity_list[i][j] < peer_min:
                    peer_min = self.peer_intensity_list[i][j]
                if self.peer_intensity_list[i][j] > peer_max:
                    peer_max = self.peer_intensity_list[i][j]

        peer_intensity=np.array(self.peer_intensity_list)
        self.peer_intensity=peer_intensity/peer_max





    def plot_gradient(self):
        #...........  GRADIENTS ...........
        ax1[0].cla()
        ax1[0].grid(False)
        ax1[0].plot(self.agent_x_gradient_position,self.agent_y_gradient_position,'go')
        ax1[0].set_title("Temperature")
        ax1[0].pcolormesh(self.x_mesh_temperature,self.y_mesh_temperature,self.temperature_intensity, cmap = plt.get_cmap('bwr'))

        ax1[1].cla()
        ax1[1].grid(False)
        ax1[1].plot(self.agent_x_gradient_position,self.agent_y_gradient_position,'go')
        ax1[1].set_title("Food")
        ax1[1].pcolormesh(self.x_mesh_food,self.y_mesh_food,self.food_intensity, cmap = plt.get_cmap('Reds'))

        ax1[2].cla()
        ax1[2].grid(False)
        ax1[2].plot(self.agent_x_gradient_position,self.agent_y_gradient_position,'go')
        ax1[2].set_title("Hydration")
        ax1[2].pcolormesh(self.x_mesh_thirst,self.y_mesh_thirst,self.thirst_intensity, cmap = plt.get_cmap('Blues'))

        ax1[3].cla()
        ax1[3].grid(False)
        ax1[3].plot(self.agent_x_gradient_position,self.agent_y_gradient_position,'go')
        ax1[3].plot(self.predator_x_gradient_position,self.predator_y_gradient_position,'ro')
        ax1[3].set_title("Predator")
        ax1[3].pcolormesh(self.x_mesh_predator,self.y_mesh_predator,self.predator_intensity, cmap = plt.get_cmap('Greens_r'))

        ax1[4].cla()
        ax1[4].grid(False)
        ax1[4].plot(self.agent_x_gradient_position,self.agent_y_gradient_position,'go')
        ax1[4].plot(self.peer_x_gradient_position,self.peer_y_gradient_position,'bo')
        ax1[4].set_title("Peer")
        ax1[4].pcolormesh(self.x_mesh_peer,self.y_mesh_peer,self.peer_intensity, cmap = plt.get_cmap('Purples'))


        fig1.canvas.flush_events()


        #...........  ATTRACTOR DYNAMICS ...........

        ax2[0].cla()
        ax2[0].grid(False)
        ax2[0].set_ylim(-0.1, 1.1)
        ax2[0].set_title("Internal state")
        if len(self.aVtemperature_list) > 100:
            ax2[0].plot(self.aVtemperature_list[-100:], color='orange', label='Temperature')
            ax2[0].plot(self.aVthirst_list[-100:], color='blue', label='Hydration')
            ax2[0].plot(self.aVfood_list[-100:], color='red', label='Energy')
            ax2[0].plot(self.aVsecurity_list[-100:], color='green', label='Security')
            ax2[0].plot(self.aVpeer_list[-100:], color='purple', label='Mating')
        else:
            ax2[0].plot(self.aVtemperature_list, color='orange', label='Temperature')
            ax2[0].plot(self.aVthirst_list, color='blue', label='Hydration')
            ax2[0].plot(self.aVfood_list, color='red', label='Energy')
            ax2[0].plot(self.aVsecurity_list, color='green', label='Security')
            ax2[0].plot(self.aVpeer_list, color='purple', label='Mating')

        ax2[1].cla()
        ax2[1].grid(False)
        ax2[1].set_title("Mean Firing Rate")
        ax2[1].set_ylim(-0.1, 1.2)
        if len(self.TFtemperature_list) > 100:
            ax2[1].plot(self.TFtemperature_list[-100:], color='orange', label='Temperature')
            ax2[1].plot(self.TFthirst_list[-100:], color='blue', label='Thirst')
            ax2[1].plot(self.TFfood_list[-100:], color='red', label='Energy')
            ax2[1].plot(self.TFpredator_list[-100:], color='green', label='Security')
            ax2[1].plot(self.TFpeer_list[-100:], color='purple', label='Mating')
        else:
            ax2[1].plot(self.TFtemperature_list, color='orange', label='Temperature')
            ax2[1].plot(self.TFthirst_list, color='blue', label='Thirst')
            ax2[1].plot(self.TFfood_list, color='red', label='Energy')
            ax2[1].plot(self.TFpredator_list, color='green', label='Security')
            ax2[1].plot(self.TFpeer_list, color='purple', label='Mating')
        ax2[1].legend(loc="upper left")

        fig2.canvas.flush_events()




########################### LOCAL VIEWS ###########################

    def temperature_LV(self): #Local View
        self.q0_temperature, self.q1_temperature, self.q2_temperature, self.q3_temperature = 0,0,0,0
        self.dV_temperature = 1

        for i in range(4):
            for j in range(3):
                self.q0_temperature += self.temperature_intensity[int(self.agent_y_gradient_position) + (j + 1), int(self.agent_x_gradient_position) - (i + 1)]
                self.q1_temperature += self.temperature_intensity[int(self.agent_y_gradient_position) + (j + 1), int(self.agent_x_gradient_position) + (i + 1)]
                self.q2_temperature += self.temperature_intensity[int(self.agent_y_gradient_position) - (j + 1), int(self.agent_x_gradient_position) - (i + 1)]
                self.q3_temperature += self.temperature_intensity[int(self.agent_y_gradient_position) - (j + 1), int(self.agent_x_gradient_position) + (i + 1)]

        self.q0_temperature /= 12
        self.q1_temperature /= 12
        self.q2_temperature /= 12
        self.q3_temperature /= 12


    def thirst_LV(self):
        self.q0_thirst, self.q1_thirst, self.q2_thirst, self.q3_thirst = 0,0,0,0
        self.dV_thirst = 1

        for i in range(4):
            for j in range(3):
                self.q0_thirst += self.thirst_intensity[int(self.agent_y_gradient_position) + (j + 1), int(self.agent_x_gradient_position) - (i + 1)]
                self.q1_thirst += self.thirst_intensity[int(self.agent_y_gradient_position) + (j + 1), int(self.agent_x_gradient_position) + (i + 1)]
                self.q2_thirst += self.thirst_intensity[int(self.agent_y_gradient_position) - (j + 1), int(self.agent_x_gradient_position) - (i + 1)]
                self.q3_thirst += self.thirst_intensity[int(self.agent_y_gradient_position) - (j + 1), int(self.agent_x_gradient_position) + (i + 1)]

        self.q0_thirst /= 12
        self.q1_thirst /= 12
        self.q2_thirst /= 12
        self.q3_thirst /= 12



    def food_LV(self): #Local View
        self.q0_food, self.q1_food, self.q2_food, self.q3_food = 0,0,0,0
        self.dV_food = 1

        for i in range(4):
            for j in range(3):
                self.q0_food += self.food_intensity[int(self.agent_y_gradient_position) + (j + 1), int(self.agent_x_gradient_position) - (i + 1)]
                self.q1_food += self.food_intensity[int(self.agent_y_gradient_position) + (j + 1), int(self.agent_x_gradient_position) + (i + 1)]
                self.q2_food += self.food_intensity[int(self.agent_y_gradient_position) - (j + 1), int(self.agent_x_gradient_position) - (i + 1)]
                self.q3_food += self.food_intensity[int(self.agent_y_gradient_position) - (j + 1), int(self.agent_x_gradient_position) + (i + 1)]

        self.q0_food /= 12
        self.q1_food /= 12
        self.q2_food /= 12
        self.q3_food /= 12


    def predator_LV(self):
        self.q0_predator, self.q1_predator, self.q2_predator, self.q3_predator = 0,0,0,0
        self.dV_predator = 1

        for i in range(4):
            for j in range(3):
                self.q0_predator += self.predator_intensity[int(self.agent_y_gradient_position) + (j + 1), int(self.agent_x_gradient_position) - (i + 1)]
                self.q1_predator += self.predator_intensity[int(self.agent_y_gradient_position) + (j + 1), int(self.agent_x_gradient_position) + (i + 1)]
                self.q2_predator += self.predator_intensity[int(self.agent_y_gradient_position) - (j + 1), int(self.agent_x_gradient_position) - (i + 1)]
                self.q3_predator += self.predator_intensity[int(self.agent_y_gradient_position) - (j + 1), int(self.agent_x_gradient_position) + (i + 1)]

        self.q0_predator /= 12
        self.q1_predator /= 12
        self.q2_predator /= 12
        self.q3_predator /= 12


    def peer_LV(self):
        self.q0_peer, self.q1_peer, self.q2_peer, self.q3_peer = 0,0,0,0
        self.dV_peer = 1

        for i in range(4):
            for j in range(3):
                self.q0_peer += self.peer_intensity[int(self.agent_y_gradient_position) + (j + 1), int(self.agent_x_gradient_position) - (i + 1)]
                self.q1_peer += self.peer_intensity[int(self.agent_y_gradient_position) + (j + 1), int(self.agent_x_gradient_position) + (i + 1)]
                self.q2_peer += self.peer_intensity[int(self.agent_y_gradient_position) - (j + 1), int(self.agent_x_gradient_position) - (i + 1)]
                self.q3_peer += self.peer_intensity[int(self.agent_y_gradient_position) - (j + 1), int(self.agent_x_gradient_position) + (i + 1)]

        self.q0_peer /= 12
        self.q1_peer /= 12
        self.q2_peer /= 12
        self.q3_peer /= 12


    def publish_Local_views(self):
        command_message = LocalViews()
        command_message.q0_temperature = self.q0_temperature
        command_message.q1_temperature = self.q1_temperature
        command_message.q2_temperature = self.q2_temperature
        command_message.q3_temperature = self.q3_temperature
        command_message.q0_thirst = self.q0_thirst
        command_message.q1_thirst = self.q1_thirst
        command_message.q2_thirst = self.q2_thirst
        command_message.q3_thirst = self.q3_thirst
        command_message.q0_food = self.q0_food
        command_message.q1_food = self.q1_food
        command_message.q2_food = self.q2_food
        command_message.q3_food = self.q3_food
        command_message.q0_predator = self.q0_predator
        command_message.q1_predator = self.q1_predator
        command_message.q2_predator = self.q2_predator
        command_message.q3_predator = self.q3_predator
        command_message.q0_peer = self.q0_peer
        command_message.q1_peer = self.q1_peer
        command_message.q2_peer = self.q2_peer
        command_message.q3_peer = self.q3_peer

        self.LV__publisher.publish(command_message)

    def complete_list(self, list_to_complet, list_max):
        for i in range(len(list_max) - len(list_to_complet)):
            list_to_complet.append(0)

        return list_to_complet


    def save_data(self):

        self.aVtemperature_list = self.complete_list(self.aVtemperature_list, self.agent_x_position_list)
        self.aVthirst_list = self.complete_list(self.aVthirst_list, self.agent_x_position_list)
        self.aVfood_list = self.complete_list(self.aVfood_list, self.agent_x_position_list)
        self.aVsecurity_list = self.complete_list(self.aVsecurity_list, self.agent_x_position_list)
        self.aVpeer_list = self.complete_list(self.aVpeer_list, self.agent_x_position_list)
        self.TFtemperature_list = self.complete_list(self.TFtemperature_list, self.agent_x_position_list)
        self.TFfood_list = self.complete_list(self.TFfood_list, self.agent_x_position_list)
        self.TFthirst_list = self.complete_list(self.TFthirst_list, self.agent_x_position_list)
        self.TFpredator_list = self.complete_list(self.TFpredator_list, self.agent_x_position_list)
        self.TFpeer_list = self.complete_list(self.TFpeer_list, self.agent_x_position_list)
        self.meanGrad_Temp = self.complete_list(self.meanGrad_Temp, self.agent_x_position_list)



        print(len(self.agent_x_position_list))
        print(len(self.agent_y_position_list))
        print(len(self.aVtemperature_list))
        print(len(self.aVthirst_list))
        print(len(self.aVfood_list))
        print(len(self.aVsecurity_list))
        print(len(self.aVpeer_list))
        print(len(self.TFtemperature_list))
        print(len(self.TFfood_list))
        print(len(self.TFthirst_list))
        print(len(self.TFpredator_list))
        print(len(self.TFpeer_list))
        print(len(self.meanGrad_Temp))




        csv_namefile = '/home/oguerrero/ROSworkspaces/contextual_allostasis_ws/data/data.csv'
        print(csv_namefile)
        
        with open(csv_namefile, mode='w') as csv_file:
            csv_writer = csv.DictWriter(csv_file, fieldnames=['Xposition', 'Yposition', 'aVtemperature', 'aVthirst', 'aVfood', 'aVpredator', 'aVpeer',
             'TFtemperature', 'TFthirst', 'TFfood', 'TFpredator', 'TFpeer', 'Grad_Temp'])
            csv_writer.writeheader()
            for i in range(len(self.agent_x_position_list)):
                csv_writer.writerow({'Xposition': self.agent_x_position_list[i], 'Yposition': self.agent_y_position_list[i], 'aVtemperature': self.aVtemperature_list[i], 'aVthirst': self.aVthirst_list[i],
                    'aVfood': self.aVfood_list[i], 'aVpredator': self.aVsecurity_list[i], 'aVpeer': self.aVpeer_list[i], 'TFtemperature': self.TFtemperature_list[i], 'TFthirst': self.TFthirst_list[i],
                    'TFfood': self.TFfood_list[i], 'TFpredator': self.TFpredator_list[i], 'TFpeer': self.TFpeer_list[i], 'Grad_Temp': self.meanGrad_Temp[i]})







    def parallel_loop_callback(self):
        print('Current cycle = ' + str(self.cycles))
        print('timestep in the cycle = ' + str(self.counter))
        print()

        print('Agent position X: ' + str(self.agent_x_position))
        print('Agent position Y: ' + str(self.agent_y_position))
        print('Agent orientation: ' + str(self.agent_orientation))
        print('Predator position X: ' + str(self.predator_x_position))
        print('Predator position Y: ' + str(self.predator_y_position))
        print('Predator orientation: ' + str(self.predator_orientation))
        print('Peer position X: ' + str(self.peer_x_position))
        print('Peer position Y: ' + str(self.peer_y_position))
        print('Peer orientation: ' + str(self.peer_orientation))
        print()

        self.agent_x_position_list.append(self.agent_x_position)
        self.agent_y_position_list.append(self.agent_y_position)

        self.agent_x_gradient_position = 125 + (self.agent_x_position*100)
        self.agent_y_gradient_position = 125 + (self.agent_y_position*100)

        self.predator_x_gradient_position = 125 + (self.predator_x_position*100)
        self.predator_y_gradient_position = 125 + (self.predator_y_position*100)

        self.peer_x_gradient_position = 125 + (self.peer_x_position*100)
        self.peer_y_gradient_position = 125 + (self.peer_y_position*100)

        self.create_temp_gradient(self.counter)
        self.create_thirst_gradient()
        self.create_food_gradient()
        self.create_predator_gradient()
        self.create_peer_gradient()
        self.build_gradients()

        self.temperature_LV()
        self.thirst_LV()
        self.food_LV()
        self.predator_LV()
        self.peer_LV()

        self.publish_Local_views()


        if plotting == True:
            self.plot_gradient()


        if self.simulationOn == True:
            if self.night == False:
                self.counter += 1
                if self.counter == self.timesteps_in_cycle:
                    self.night = True
                    self.cycles += 1

            else:
                self.counter -= 1
                if self.counter == 0:
                    self.night = False
                    self.cycles += 1

            if self.cycles >= self.max_cycles:
                command_message = SimulationOn()
                command_message.simulation_running = False
                self.Simulation__publisher.publish(command_message)

                self.save_data()








def main(args=None):
    rclpy.init(args=args)
    pseudo_supervisor = Pseudo_Supervisor()
    rclpy.spin(pseudo_supervisor)
    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    pseudo_supervisor.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()