import rclpy
import numpy as np
from rclpy.node import Node
from sensor_msgs.msg import Range
from webots_msgs_pkg.msg import EpuckMotors, LocalViews, InternalState, AttOutput, SimulationOn
from webots_ros2_msgs.msg import FloatStamped
import random


class Multiattractor(object):

    def __init__(self):

        '''
        PARAMETERS
        '''

        self.dt = 0.001

        # couplings
        self.we = 5  # self-exitation
        self.wsi = 4
        self.wmi = 4

        self.q = 0.8  # ACh level (0 corresponds to shared inhibition and 1 to mutual inhibition) Competition level - 0.2

        '''# external inputs
        self.I1 = -100
        self.I2 = -100'''

        # Initial state
        self.U = []

        # Initial 'dummy' variance
        self.var = 50  # noise - 50

        # time decay constants
        self.tau = 0.02

        # activation function
        self.a = 1 / 22
        self.thr = 15
        self.fmax = 40


    def sigmoid(self, x):
        return self.fmax / (1 + np.exp(-self.a * (x - self.thr)))

    # this function returns the right hand side of the modified Wilson-Cowan equation
    def WilsonCowan(self, input_list):

        y_list = []

        for i in range(len(input_list)):
            I = input_list[i]
            wU = self.we * self.U[i]
            inh1 = self.q * self.wmi * (sum(self.U) - self.U[i])
            inh2 = (1 - self.q) * self.wsi * self.sigmoid(sum(self.U))
            noise = np.random.normal(0, 1 * self.var)

            y_list.append((-self.U[i] + self.sigmoid(wU - inh1 - inh2 + I) + noise) / self.tau)

        return np.array(y_list)

    def advance(self, *input_list):

        if self.U == []:
            self.U = [0] * len(input_list)

        y = self.WilsonCowan(input_list)

        for i in range(len(input_list)):
            self.U[i] += y[i] * self.dt
            self.U[i] *= self.U[i] > 0

        return self.U


class RandomWalker(Node):
    def __init__(self):
        super().__init__('allostatic_controller')

        self.multiattractor = Multiattractor()
        self.att_normal_factor = 20

        self.dV = 1.0
        self.action_interval_counter = 0
        self.wheel_speed_r, self.wheel_speed_l = 3.0, 3.0
        self.ps0, self.ps1, self.ps2, self.ps3, self.ps4, self.ps5, self.ps6, self.ps7 = 0,0,0,0,0,0,0,0
        self.aV_temperature = 0.0
        self.aV_thirst = 0.0
        self.aV_food = 0.0
        self.aV_predator = 0.0
        self.aV_peer = 0.0
        self.diff_temperature = 1.0
        self.diff_thirst = 1.0
        self.diff_food = 1.0
        self.diff_predator = 1.0
        self.diff_peer = 1.0
        self.hsign_temperature = 0
        self.hsign_thirst = 0
        self.hsign_food = 0
        self.hsign_predator = 0
        self.hsign_peer = 0

        self.aVhomeo_temperature = 0.5
        self.aVhomeo_thirst = 0.8
        self.aVhomeo_food = 1.0
        self.aVhomeo_peer = 0.4

        self.agent_orientation = 0

        self.counter = 0

        self.wheel_r = 0.1
        self.wheel_l = 0.1


        self.get_logger().info('------------- ----------------- allostatic_controller -------------- -------------')

        self.Motor__publisher = self.create_publisher(EpuckMotors, 'allostatic_controller/epuck_motors', 1)
        self.Homeostasis__publisher = self.create_publisher(InternalState, 'allostatic_controller/InternalState', 1)
        self.Attractor__publisher = self.create_publisher(AttOutput, 'allostatic_controller/Attractor_outputs', 1)
        self.Simulation__publisher = self.create_publisher(SimulationOn, 'allostatic_controller/SimulationRuning', 1)
        


        self.create_subscription(Range, 'epuck_agent/ps0', self.__ps0_sensor_callback, 1)
        self.create_subscription(Range, 'epuck_agent/ps1', self.__ps1_sensor_callback, 1)
        self.create_subscription(Range, 'epuck_agent/ps2', self.__ps2_sensor_callback, 1)
        self.create_subscription(Range, 'epuck_agent/ps3', self.__ps3_sensor_callback, 1)
        self.create_subscription(Range, 'epuck_agent/ps4', self.__ps4_sensor_callback, 1)
        self.create_subscription(Range, 'epuck_agent/ps5', self.__ps5_sensor_callback, 1)
        self.create_subscription(Range, 'epuck_agent/ps6', self.__ps6_sensor_callback, 1)
        self.create_subscription(Range, 'epuck_agent/ps7', self.__ps7_sensor_callback, 1)

        self.create_subscription(FloatStamped, '/my_epuck/compass/bearing', self.agent_Compass_callback, 1)
        self.create_subscription(LocalViews, 'pseudo_supervisor/Local_views', self.LV__callback, 1)

        self.timer = self.create_timer(0.05, self.parallel_loop_callback)

        
        
    def __ps0_sensor_callback(self, message):
        self.ps0 = message.range

    def __ps1_sensor_callback(self, message):
        self.ps1 = message.range

    def __ps2_sensor_callback(self, message):
        self.ps2 = message.range

    def __ps3_sensor_callback(self, message):
        self.ps3 = message.range

    def __ps4_sensor_callback(self, message):
        self.ps4 = message.range

    def __ps5_sensor_callback(self, message):
        self.ps5 = message.range

    def __ps6_sensor_callback(self, message):
        self.ps6 = message.range


    def detect_obstacle(self):
        if any(sensor < 0.29  for sensor in self.prox_sensors):
            self.obstacle = True
            if self.__right_sensor_value > self.__left_sensor_value:
                self.obstacle_right = True

            else:
                self.obstacle_right = False

        else:
            self.obstacle = False


    def obstacle_avoidance(self):
        if self.obstacle == True:
            if self.obstacle_right == False:
                if self.wheel_speed_r < 6:
                    self.wheel_speed_r += 0.1
                if self.wheel_speed_l > 3:
                    self.wheel_speed_l -= 0.1

            else:
                if self.wheel_speed_l < 6:
                    self.wheel_speed_l += 0.1
                if self.wheel_speed_r > 3:
                    self.wheel_speed_r -= 0.1


    def random_action(self, action_interval):

        self.action_interval_counter += 1
        if self.action_interval_counter > action_interval:

            self.detect_obstacle()
            if self.obstacle == True:
                self.obstacle_avoidance()

            else:
                    
                self.action_interval_counter = 0
                self.wheel_speed_r = self.wheel_speed_r + random.gauss(0,1)
                self.wheel_speed_l = self.wheel_speed_l + random.gauss(0,1)

                if self.wheel_speed_r < 3.0: self.wheel_speed_r = 3.0
                if self.wheel_speed_r > 6.0: self.wheel_speed_r = 6.0
                if self.wheel_speed_l < 3.0: self.wheel_speed_l = 3.0
                if self.wheel_speed_l > 6.0: self.wheel_speed_l = 6.0

        return(self.wheel_speed_r, self.wheel_speed_l)


    def navigation(self, action_interval):
        self.action_interval_counter += 1
        if self.action_interval_counter > action_interval:

            self.detect_obstacle()
            if self.obstacle == True:
                self.obstacle_avoidance()

            else:
                self.action_interval_counter = 0
                self.wheel_speed_r = self.wheel_r
                self.wheel_speed_l = self.wheel_l

        return(self.wheel_speed_r, self.wheel_speed_l)






    def __ps7_sensor_callback(self, message):
        command_message = EpuckMotors()

        self.ps7 = message.range

        self.prox_sensors = [self.ps0, self.ps1, self.ps2, self.ps3, self.ps4, self.ps5, self.ps6, self.ps7]

        self.__right_sensor_value = (self.ps0 + self.ps1 + self.ps2 + self.ps3) / 4
        self.__left_sensor_value = (self.ps4 + self.ps5 + self.ps6 + self.ps7) / 4

        #command_message.right_motor, command_message.left_motor = self.random_action(action_interval = 5)
        command_message.right_motor, command_message.left_motor = self.navigation(action_interval = 5)

        self.Motor__publisher.publish(command_message)


    def agent_Compass_callback(self, message):
        self.agent_orientation = message.data


    def LV__callback(self, message):

        self.q0_temperature = message.q0_temperature
        self.q1_temperature = message.q1_temperature
        self.q2_temperature = message.q2_temperature
        self.q3_temperature = message.q3_temperature
        self.aV_temperature = (self.q0_temperature + self.q1_temperature + self.q2_temperature + self.q3_temperature) / 4
        self.diff_temperature = abs(self.dV - self.aV_temperature)

        self.q0_thirst = message.q0_thirst
        self.q1_thirst = message.q1_thirst
        self.q2_thirst = message.q2_thirst
        self.q3_thirst = message.q3_thirst
        self.aV_thirst = (self.q0_thirst + self.q1_thirst + self.q2_thirst + self.q3_thirst) / 4
        self.diff_thirst = abs(self.dV - self.aV_thirst)

        self.q0_food = message.q0_food
        self.q1_food = message.q1_food
        self.q2_food = message.q2_food
        self.q3_food = message.q3_food
        self.aV_food = (self.q0_food + self.q1_food + self.q2_food + self.q3_food) / 4
        self.diff_food = abs(self.dV - self.aV_food)

        self.q0_predator = message.q0_predator
        self.q1_predator = message.q1_predator
        self.q2_predator = message.q2_predator
        self.q3_predator = message.q3_predator
        self.aV_predator = (self.q0_predator + self.q1_predator + self.q2_predator + self.q3_predator) / 4
        self.diff_predator = abs(0 - self.aV_predator)

        self.q0_peer = message.q0_peer
        self.q1_peer = message.q1_peer
        self.q2_peer = message.q2_peer
        self.q3_peer = message.q3_peer
        self.aV_peer = (self.q0_peer + self.q1_peer + self.q2_peer + self.q3_peer) / 4
        self.diff_peer =abs (self.dV - self.aV_peer)

        print('Local view for Temperature = ' + str(self.aV_temperature))
        print('Local view for Thirst = ' + str(self.aV_thirst))
        print('Local view for Food = ' + str(self.aV_food))
        print('Local view for Predator = ' + str(self.aV_predator))
        print('Local view for Peer = ' + str(self.aV_peer))
        print()


########################### ORIENTATION ###########################

    def adsign(self):
        self.adsign_temperature = np.sign(self.dV - self.aV_temperature)
        self.adsign_thirst = np.sign(self.dV - self.aV_thirst)
        self.adsign_food = np.sign(self.dV - self.aV_food)
        self.adsign_predator = np.sign(0 - self.aV_predator)
        self.adsign_peer = np.sign(self.dV - self.aV_peer)


    def hsign(self):
        theta = self.agent_orientation
        if theta <= 292 and theta > 247: #UP
            self.hsign_temperature = np.sign(self.q0_temperature - self.q1_temperature)
            self.hsign_thirst = np.sign(self.q0_thirst - self.q1_thirst)
            self.hsign_food = np.sign(self.q0_food - self.q1_food)
            self.hsign_predator = np.sign(self.q0_predator - self.q1_predator)
            self.hsign_peer = np.sign(self.q0_peer - self.q1_peer)

        elif theta <= 247 and theta > 202: #UP-L
            self.hsign_temperature = np.sign(((self.q0_temperature + self.q2_temperature)/2) - ((self.q0_temperature + self.q1_temperature)/2)) 
            self.hsign_thirst = np.sign(((self.q0_thirst + self.q2_thirst)/2) - ((self.q0_thirst + self.q1_thirst)/2)) 
            self.hsign_food = np.sign(((self.q0_food + self.q2_food)/2) - ((self.q0_food + self.q1_food)/2)) 
            self.hsign_predator = np.sign(((self.q0_predator + self.q2_predator)/2) - ((self.q0_predator + self.q1_predator)/2)) 
            self.hsign_peer = np.sign(((self.q0_peer + self.q2_peer)/2) - ((self.q0_peer + self.q1_peer)/2)) 

        elif theta <= 202 and theta > 157: #L
            self.hsign_temperature = np.sign(self.q2_temperature - self.q0_temperature)
            self.hsign_thirst = np.sign(self.q2_thirst - self.q0_thirst)
            self.hsign_food = np.sign(self.q2_food - self.q0_food)
            self.hsign_predator = np.sign(self.q2_predator - self.q0_predator)
            self.hsign_peer = np.sign(self.q2_peer - self.q0_peer)

        elif theta <= 157 and theta > 112: #DOWN-L
            self.hsign_temperature = np.sign(((self.q2_temperature + self.q3_temperature)/2) - ((self.q2_temperature + self.q0_temperature)/2))
            self.hsign_thirst = np.sign(((self.q2_thirst + self.q3_thirst)/2) - ((self.q2_thirst + self.q0_thirst)/2))
            self.hsign_food = np.sign(((self.q2_food + self.q3_food)/2) - ((self.q2_food + self.q0_food)/2))
            self.hsign_predator = np.sign(((self.q2_predator + self.q3_predator)/2) - ((self.q2_predator + self.q0_predator)/2))
            self.hsign_peer = np.sign(((self.q2_peer + self.q3_peer)/2) - ((self.q2_peer + self.q0_peer)/2))

        elif theta <= 112 and theta > 77: #DOWN
            self.hsign_temperature = np.sign(self.q3_temperature - self.q2_temperature)
            self.hsign_thirst = np.sign(self.q3_thirst - self.q2_thirst)
            self.hsign_food = np.sign(self.q3_food - self.q2_food)
            self.hsign_predator = np.sign(self.q3_predator - self.q2_predator)
            self.hsign_peer = np.sign(self.q3_peer - self.q2_peer)

        elif theta <= 77 and theta > 22: #DOWN-R
            self.hsign_temperature = np.sign(((self.q3_temperature + self.q1_temperature)/2) - ((self.q3_temperature + self.q2_temperature)/2))
            self.hsign_thirst = np.sign(((self.q3_thirst + self.q1_thirst)/2) - ((self.q3_thirst + self.q2_thirst)/2))
            self.hsign_food = np.sign(((self.q3_food + self.q1_food)/2) - ((self.q3_food + self.q2_food)/2))
            self.hsign_predator = np.sign(((self.q3_predator + self.q1_predator)/2) - ((self.q3_predator + self.q2_predator)/2))
            self.hsign_peer = np.sign(((self.q3_peer + self.q1_peer)/2) - ((self.q3_peer + self.q2_peer)/2))

        elif theta <= 22 and theta > 337: #R
            self.hsign_temperature = np.sign(self.q1_temperature - self.q3_temperature)
            self.hsign_thirst = np.sign(self.q1_thirst - self.q3_thirst)
            self.hsign_food = np.sign(self.q1_food - self.q3_food)
            self.hsign_predator = np.sign(self.q1_predator - self.q3_predator)
            self.hsign_peer = np.sign(self.q1_peer - self.q3_peer)

        elif theta <= 337 and theta > 292: #UP-R
            self.hsign_temperature = np.sign(((self.q1_temperature + self.q0_temperature)/2) - ((self.q1_temperature + self.q3_temperature)/2))
            self.hsign_thirst = np.sign(((self.q1_thirst + self.q0_thirst)/2) - ((self.q1_thirst + self.q3_thirst)/2))
            self.hsign_food = np.sign(((self.q1_food + self.q0_food)/2) - ((self.q1_food + self.q3_food)/2))
            self.hsign_predator = np.sign(((self.q1_predator + self.q0_predator)/2) - ((self.q1_predator + self.q3_predator)/2))
            self.hsign_peer = np.sign(((self.q1_peer + self.q0_peer)/2) - ((self.q1_peer + self.q3_peer)/2))


########################### HOMEOSTASIS ###########################

    def homeostasis(self):
        temperature_discount = 0.0005 #0.001
        thirst_discount = 0.0002 #0.001
        food_discount = 0.0002 #0.001
        peer_discount = 0.0001 #0.001

        bonus = 0.01

        self.aVhomeo_temperature -= temperature_discount
        self.aVhomeo_thirst -= thirst_discount
        self.aVhomeo_food -= food_discount
        self.aVhomeo_peer -= peer_discount
        self.aVhomeo_predator = self.aV_predator

        if self.aVhomeo_temperature < 0: self.aVhomeo_temperature = 0.0
        if self.aVhomeo_thirst < 0: self.aVhomeo_thirst = 0.0
        if self.aVhomeo_food < 0: self.aVhomeo_food = 0.0
        if self.aVhomeo_peer < 0: self.aVhomeo_peer = 0.0


        
        if self.diff_temperature<0.5:
            self.aVhomeo_temperature += bonus

        if self.diff_thirst<0.05:
            self.aVhomeo_thirst += bonus

        if self.diff_food<0.05:
            self.aVhomeo_food += bonus

        if self.diff_peer<0.05:
            self.aVhomeo_peer += bonus



        if self.aVhomeo_temperature > 1: self.aVhomeo_temperature = 1.0
        if self.aVhomeo_thirst > 1: self.aVhomeo_thirst = 1.0
        if self.aVhomeo_food > 1: self.aVhomeo_food = 1.0
        if self.aVhomeo_peer > 1: self.aVhomeo_peer = 1.0

        #The attractor input will be 1 - the Actual value, resulting in 0 when the system is satisfied
        self.Itemp_attractor = 1 - self.aVhomeo_temperature
        self.Ithi_attractor = 1 - self.aVhomeo_thirst
        self.Ifood_attractor = 1 - self.aVhomeo_food
        self.Ipeer_attractor = 1 - self.aVhomeo_peer
        self.Ipredator_attractor = self.aVhomeo_predator


        command_message = InternalState()
        command_message.is_temperature = self.aVhomeo_temperature
        command_message.is_thirst = self.aVhomeo_thirst
        command_message.is_food = self.aVhomeo_food
        command_message.is_predator = self.aVhomeo_predator
        command_message.is_peer = self.aVhomeo_peer

        self.Homeostasis__publisher.publish(command_message)


    def attractor_dynamics(self):

        self.Itemp_attractor *= 20
        self.Ithi_attractor *= 20
        self.Ifood_attractor *= 20
        self.Ipeer_attractor *= 20
        self.Ipredator_attractor *= 20
        self.total_force_temperature, self.total_force_thirst, self.total_force_food, self.total_force_predator, self.total_force_peer  = self.multiattractor.advance(self.Itemp_attractor, self.Ithi_attractor, self.Ifood_attractor, self.Ipeer_attractor, self.Ipredator_attractor)

        att_outputs = [self.total_force_temperature, self.total_force_thirst, self.total_force_food, self.total_force_predator, self.total_force_peer]

        if any(output < self.att_normal_factor  for output in att_outputs):
            self.att_normal_factor = max(att_outputs)


        self.total_force_temperature /= self.att_normal_factor
        self.total_force_thirst /= self.att_normal_factor
        self.total_force_food /= self.att_normal_factor
        self.total_force_predator /= self.att_normal_factor
        self.total_force_peer /= self.att_normal_factor


        command_message = AttOutput()
        command_message.att_temperature = self.total_force_temperature
        command_message.att_thirst = self.total_force_thirst
        command_message.att_food = self.total_force_food
        command_message.att_predator = self.total_force_predator
        command_message.att_peer = self.total_force_peer

        self.Attractor__publisher.publish(command_message)


    def wheel_turning(self):
        self.wheel_r = 1 + ((self.hsign_temperature * self.adsign_temperature* self.total_force_temperature) + (self.hsign_thirst * self.adsign_thirst* self.total_force_thirst) + (self.hsign_food * self.adsign_food* self.total_force_food) + (self.hsign_predator * self.adsign_predator* self.total_force_predator) + (self.hsign_peer * self.adsign_peer* self.total_force_peer)) * (1/5)
        self.wheel_l = 1 - ((self.hsign_temperature * self.adsign_temperature* self.total_force_temperature) + (self.hsign_thirst * self.adsign_thirst* self.total_force_thirst) + (self.hsign_food * self.adsign_food* self.total_force_food) + (self.hsign_predator * self.adsign_predator* self.total_force_predator) + (self.hsign_peer * self.adsign_peer* self.total_force_peer)) * (1/5)
        self.wheel_r *= 5 + np.random.uniform(-0.5,0.5)
        self.wheel_l *= 5 + np.random.uniform(-0.5,0.5)

        if self.wheel_r > 6.2: self.wheel_r = 6.2
        if self.wheel_l > 6.2: self.wheel_l = 6.2




    def parallel_loop_callback(self):
        self.counter += 1
        if self.counter >= 20:
            if self.counter == 20:
                command_message = SimulationOn()
                command_message.simulation_running = True
                self.Simulation__publisher.publish(command_message)

            self.adsign()
            self.hsign()
            self.homeostasis()
            self.attractor_dynamics()
            self.wheel_turning()




def main(args=None):
    rclpy.init(args=args)
    walker = RandomWalker()
    rclpy.spin(walker)
    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    walker.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()